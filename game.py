from random import randint

name=input("hi what's your name ?")
for guesses in range(1,6):
    birth_month = randint(1,12)
    birth_year = randint(1924,2004)
    print ("Guess", guesses, ": Hello",name, "where you born on ",birth_month,"month of", birth_year)
    answer=input("yes or no ?")
    if answer =="yes":
        print("I knew it!")
        exit()
    elif answer == "no":
        print("Drat! Lemme try again!")
